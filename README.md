# Overview

stypi-get is a command line utility for fetching files from [code.stypi.com](https://code.stypi.com/)

Simply write a file called `stypi-get-config` in your working directory and run the script.

The format of the file is as follows:

```
USERNAME
directory/filename
filename2
etc
```

# Authors

* [Mathew Campbell](mailto:mathewcampbell.is+stypi-get@gmail.com)