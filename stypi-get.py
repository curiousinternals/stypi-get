#!/usr/bin/python

import argparse
import json
from jsonschema import Draft4Validator
import urllib

gDefaultConfigFilename = "stypi-get.json"
gVersion = "0.1.0"

def loadConfig():
  schemaName = "../config-schema.json"

  with open(gDefaultConfigFilename) as f:
    rawConfig = f.read()

  with open(schemaName) as f:
    rawSchema = f.read()

  config = json.loads(rawConfig)
  schema = json.loads(rawSchema)
  print(Draft4Validator(schema).is_valid(config))

  return config

def fetchAndSave(username, root, filename, stripRoot):
  stypiUrl = "https://code.stypi.com"

  if not(stripRoot):
    filename = root + "/" + filename

  url = stypiUrl + "/raw/" + username + "/" + filename

  print("  Fetching: " + url)
  f = urllib.urlopen(url)
  raw = f.read()
  f.close()

  print("  Writing: " + filename)
  with open(filename, "w") as f:
    f.write(raw)

# Sets up an initial config file
def init(args):
  print("Initializing config file: " + gDefaultConfigFilenames)

  with open("../init.json") as i:
    with open(gDefaultConfigFilename, "w") as f:
      f.write(i.read())

# The normal operation to perform, fetching args
def fetch(args):
  # Init
  config = loadConfig()

  # Download and save the files
  for u in range(len(config)):
    for p in range(len(config[u]["projects"])):
      for f in range(len(config[u]["projects"][p]["files"])):
        fetchAndSave(
          config[u]["user"],
          config[u]["projects"][p]["root"],
          config[u]["projects"][p]["files"][f],
          config[u]["projects"][p]["strip root"]
        )

def main():
  # Set up argument parsing
  parser = argparse.ArgumentParser(
    prog="stypi-get",
    description="Get files from code.stypi.com"
  )
  parser.add_argument(
    "--version", action="version", version="%(prog)s v" + gVersion
  )

  subparsers = parser.add_subparsers(
    help="The commands available to %(prog)s"
  )

  # fetch args
  pFetch = subparsers.add_parser(
    "fetch", help="Fetch some files from code.stypi.com"
  )
  pFetch.set_defaults(func=fetch)
  pFetch.add_argument(
    "--strip-root", dest="stripRoot", action="store_const",
    const=True, default=False,
    help="strip the root from the destination files (default: False)"
  )
  pFetch.add_argument("-user", nargs="?", help="The username to fetch from")
  pFetch.add_argument("-root", nargs="?", help="The root directory to use")
  pFetch.add_argument("-file", nargs="+", help="A list of filenames to fetch")
  pFetch.add_argument("-config", nargs="?", help="A config file to use")

  # init args
  pInit = subparsers.add_parser(
    "init", help="Create a simple example config file here"
  )
  pInit.set_defaults(func=init)

  args = parser.parse_args()

  print(args)

main()